'use strict';

(function(){


	angular.module('myapp')
		.component('counter' , counter())
		.controller('CounterCmpCtrl', CounterCmpCtrl);

	function counter(){
		return {
			bindings: {
				count: '='
			},
			// el controlador de los componentes no necesita un nombre, 
			//por defecto se puede usar $ctrl
			//controller: 'CounterCmpCtrl as counterCmpVM',
			controller: 'CounterCmpCtrl',
			template: [
				'<div>',
				//'	<input type = "number" ng-model="counterCmpVM.count"></input>',
				'	<input type = "number" ng-model="$ctrl.count"></input>',
				'	<button ng-click="$ctrl.decrement();">-</button>',
				'	<button ng-click="$ctrl.increment();">+</button>',
				'</div>'
			].join('')

		}
	}

	CounterCmpCtrl.$inject = [];
	function CounterCmpCtrl(){
		var vm = this;
		vm.decrement = _decrement;
		vm.increment = _increment;// así vemos todas las funciones que hay 

		function _decrement() {
			vm.count--;
		}

		function _increment() {
			vm.count++;
		}
	}
}())