'use strict';
(function() {
	angular.module('myapp')
	.directive('counter', counter)
	.controller('CounterDirectiveController', CounterDirectiveController);

	function counter(){
		return {
			scope: {
			},
			bindToController: {// para poder incluir la variable en el controlador y no usar el scope
				count: '='
			},
			controller: 'CounterDirectiveController as counterDirectiveVM',
	    	template: [
				'<div>',
				'	<input type = "number" ng-model="counterDirectiveVM.count"></input>',
				'	<button ng-click="counterDirectiveVM.decrement();">-</button>',
				'	<button ng-click="counterDirectiveVM.increment();">+</button>',
				'</div>'
			].join('')
	  };
	}

	CounterDirectiveController.$inject = [];
	function CounterDirectiveController() {
		var vm = this;
		vm.decrement = _decrement;
		vm.increment = _increment;

		function _decrement() {
			vm.count--;
		}

		function _increment() {
			vm.count++;
		}
	}
}());