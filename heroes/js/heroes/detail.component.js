'use strict';
(function() {
	angular.module('heroes')
	.component('detail' , detail())
	.controller('DetailHeroController', DetailHeroController);
	
	DetailHeroController.$inject = [];

	function detail(){
		return {
			bindings: {
				hero: '<',
				onDelete: '&',
 			 	onUpdate: '&'
			},
			// el controlador de los componentes no necesita un nombre, 
			//por defecto se puede usar $ctrl
			//controller: 'CounterCmpCtrl as counterCmpVM',
			controller: 'DetailHeroController',
			template: [
				'<div>',
				//'	<input type = "number" ng-model="counterCmpVM.count"></input>',
				'	Name:{{$ctrl.hero.name}} <br>',
				'	Location: ',
				' <editable editlocation="$ctrl.hero.location"></editable>',
				' <button ng-click="$ctrl.delete()">Delete</button>',
				' <hr>',
				'</div>'
			].join('')

		}
	}

	function DetailHeroController (){
		var vm = this;

		vm.delete = function() {
		    vm.onDelete({hero: vm.hero});
		};

		vm.update = function(prop, value) {
		    vm.onUpdate({hero: vm.hero, prop: prop, value: value});
		};
	}


}());