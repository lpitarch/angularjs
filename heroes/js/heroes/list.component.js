'use strict';

(function(){
	angular.module('heroes')
		.component('list' , list())
		.controller('ListController', ListController);

	function list(){
		return {
			bindings: {
			},
			// el controlador de los componentes no necesita un nombre, 
			//por defecto se puede usar $ctrl
			//controller: 'CounterCmpCtrl as counterCmpVM',
			controller: 'ListController',
			template: [
				'<h2>Heroes</h2>',
				'<div>',
				//'	<input type = "number" ng-model="counterCmpVM.count"></input>',
				'	<detail ng-repeat="hero in $ctrl.heroes" hero="hero"',
				' on-delete="$ctrl.deleteHero(hero)" on-update="$ctrl.updateHero(hero, prop, value)"></detail>',
				'</div>'
			].join('')

		}
	}

	ListController.$inject = [];

	function ListController(){
		
		var vm = this;

		vm.heroes = [
			{ id:0, name: "Superman", location: "Smallville"}, 
			{ id:1, name: "Batman", location: "Gotham"}, 
			{ id:2, name: "Daredevil", location: "New York"}
		];

		vm.updateHero = function(hero, prop, value){
			hero[prop] = value;
		}

		vm.deleteHero = function(hero){
			var idx = vm.heroes.indexOf(hero);
		    if (idx >= 0) {
		      vm.heroes.splice(idx, 1);
		    }
		}
	}
}())