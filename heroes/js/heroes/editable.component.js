'use strict';
(function() {
	angular.module('heroes')
	.component('editable' , editable())
	.controller('EditableHeroController', EditableHeroController);
	
	function editable(){
		return {
			bindings: {
				editlocation: '<',
				onDelete: '&',
 			 	onUpdate: '&'
			},
			// el controlador de los componentes no necesita un nombre, 
			//por defecto se puede usar $ctrl
			//controller: 'CounterCmpCtrl as counterCmpVM',
			controller: 'EditableHeroController',
			template: [
				'<span ng-switch="$ctrl.editMode">',
 					'<input ng-switch-when="true" type="text" ng-model="$ctrl.editlocation">',
  					'<span ng-switch-default>{{$ctrl.editlocation}}</span>',
				'</span>',
				'<button ng-click="$ctrl.handleModeChange()">{{$ctrl.editMode ? "Guardar" : "Editar"}}</button>',
				'<button ng-if="$ctrl.editMode" ng-click="$ctrl.reset()">Reset</button>'
			].join('')

		}
	}
	
	function EditableHeroController(){
		var vm = this;
  		vm.editMode = false;
  		vm.originalValue;
  		vm.reset = _reset;

  		vm.$onInit = function (){
  			vm.originalValue = vm.editlocation;
  		}

  		function _reset(){
  			vm.editlocation = vm.originalValue;
  		}

  		vm.handleModeChange = function(){
			if(!vm.editMode){
  				vm.editMode = true;
  				vm.onUpdate({value: vm.editlocation});
  			}else{	
	  			vm.editMode = false;
  			}
  		}

	}

}());