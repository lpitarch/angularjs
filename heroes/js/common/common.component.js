'use strict';

(function(){

	angular.module('common')
		.component('header' , header())
		.component('footer' , footer())
		.controller('CommonCmpCtrl', CommonCmpCtrl);

	function header(){
		return {
			bindings: {
				count: '='
			},
			// el controlador de los componentes no necesita un nombre, 
			//por defecto se puede usar $ctrl
			//controller: 'CounterCmpCtrl as counterCmpVM',
			controller: 'CommonCmpCtrl',
			template: [
				'<h1> My first app with components</h1>'
			].join('')

		}
	}

	function footer(){
		return {
			bindings: {
				count: '='
			},
			// el controlador de los componentes no necesita un nombre, 
			//por defecto se puede usar $ctrl
			//controller: 'CounterCmpCtrl as counterCmpVM',
			controller: 'CommonCmpCtrl',
			template: [
				'<h3> Developed by Luis Pitarch</h3>'
			].join('')

		}
	}
	CommonCmpCtrl.$inject = [];

	function CommonCmpCtrl(){
		
	}
}())